Great Old Bot
=============

This is a simple Mastodon bot that posts random lines from H.P. Lovecraft's public domain works.

You can see it in action here: https://botsin.space/@GreatOldBot/

This is a WIP

Lovecraft text is derived from the Project Gutenberg versions: https://www.gutenberg.org/
