#!/usr/bin/env python
"""
Linify

Usage:
    linefy <infile>

"""

import re
from docopt import docopt


def read_file(the_file):
    with open(the_file, 'r') as infile:
        return infile.read()

def write_file(the_file, text):
    with open(the_file, w) as outfile:
        outfile.write(text)


def fix_text(text):
    # replace all newlines with spaces
    text = re.sub(r'[\r\n]+', ' ', text)

    # remove redundant spaces
    text = re.sub(r'  +', ' ', text)

    # split lines on . ! ?
    text = re.sub(r'! ', '!\n', text)
    text = re.sub(r'\? ', '?\n', text)

    # try to avoid spliiting on W. Latitude and 3 p. m.
    text = re.sub('( \w\w+)\. ', '\g<1>.\n', text)

    return text

if __name__ == '__main__':
    arguments = docopt(__doc__, version='Linefy 0.1')

    infile = arguments['<infile>']

    # foo = "Hi there. Hey, ho! Our Dr. Smith\n went to W. 57th street at 3 p. m. on\n Wednesday? Capt. Murphy farted."

    # print(fix_text(foo))

    text = read_file(infile)
    text = fix_text(text)
    print(text)