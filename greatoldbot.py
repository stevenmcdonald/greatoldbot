#!/usr/bin/env python
"""
GreatOldBot

Usage:
    greatoldbot
    greatoldbot (-h | --help)
    greatoldbot (-v | --verbose)
    greatoldbot (-d | --dryrun)

Options:
    -d --dryrun     Don't actually post
    -h --help       Show this screen
    -v --verbose    Verbose output

"""

import hashlib
import json
import random
import requests

from docopt import docopt
from pprint import pprint

from secret import API_TOKEN, SERVER


HASHTAGS = '#Lovecraft #HPLovecraft #CthulhuMythos #horror #bot'

def read_corpus():
    with open('corpus.txt') as infile:
        temp = infile.readlines()
        lines = list(map(lambda x: x.rstrip(), temp))
        return lines

def get_post(status):
    return {
        'status': status,
        'visibility': 'public',
        'language': 'en',
    }

def post_line(line, digest, dryrun=False):
    headers = {
        'Authorization': f'Bearer {API_TOKEN}',
        'Idempotency-Key': digest,
        'Content-Type': 'application/json; charset=utf8'
    }

    body = get_post(line)

    url = f'https://{SERVER}/api/v1/statuses'

    if dryrun:
        print(url)
        pprint(headers)
        pprint(body)
    else:
        r = requests.post(url, headers=headers, data=json.dumps(body))

        print(r.status_code)
        print(r.json())


if __name__ == '__main__':
    arguments = docopt(__doc__, version='GreatOldBot 0.1')

    dryrun = arguments['--dryrun']

    # only post every other time we're called
    if random.choice([True, False]):
        lines = read_corpus()
        line = random.choice(lines)

        # line += '\n\n' + HASHTAGS

        digest = hashlib.sha256(line.encode('utf-8')).hexdigest()

        # print(arguments)

        post_line(line, digest, dryrun=dryrun)
    else:
        print('skipping')
